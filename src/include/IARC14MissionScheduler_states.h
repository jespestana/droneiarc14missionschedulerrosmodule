#ifndef DRONEIARC14MISSIONSCHEDULER_STATES_H
#define DRONEIARC14MISSIONSCHEDULER_STATES_H

namespace MissionSchedulerStates {
    enum StateType {
        INITIAL_STATE                         =  0,
        INIT_SEQUENCE                         =  1,
        HOVERING                              =  2,
        CONTROL_STARTUP_SEQUENCE              =  3,
        TO_IN_FLIGHT_BORDER_BOUNCING_SEQUENCE =  4,
        IN_FLIGHT_BORDER_BOUNCING             =  5,
        TO_PBVS_TRACKER_IS_REFERENCE_SEQUENCE =  6,
        PBVS_TRACKER_IS_REFERENCE             =  7,
        TO_PBVS_TRACKER_IS_FEEDBACK_SEQUENCE  =  8,
        PBVS_TRACKER_IS_FEEDBACK              =  9,
        TO_MOVE_TOWARDS_TOUCH_DOWN_SEQUENCE   = 10,
        MOVE_TOWARDS_TOUCH_DOWN               = 11,
        TOUCH_DOWN_SEQUENCE                   = 12,
        FROM_TOUCH_DOWN_SEQUENCE              = 13
    };
}

#endif // DRONEIARC14MISSIONSCHEDULER_STATES_H
