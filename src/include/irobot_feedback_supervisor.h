#ifndef IROBOT_FEEDBACK_SUPERVISOR_H
#define IROBOT_FEEDBACK_SUPERVISOR_H

#include "tracked_irobot.h"
#include <list>
#include "droneMsgsROS/robotPoseStampedVector.h"
#include "communication_definition.h"
#include "cvg_utils_library.h"

#define IRFS_FORBIDDEN_ANGLE_RANGE_COLUMN ( 200.0 )  // deg

class IRobotFeedbackSupervisor
{
private:
    ros::NodeHandle n;

public:
    IRobotFeedbackSupervisor();
    ~IRobotFeedbackSupervisor();

    void open(ros::NodeHandle & nIn);

private:
    std::list<TrackedIRobot> irobot_list;

    // Subscription to irobot tracking feedback
    ros::Subscriber iRobotsPoseTrackerFeedbackSubs;
    void iRobotsPoseTrackerPoseCallback(const droneMsgsROS::robotPoseStampedVector::ConstPtr& msg);

public:
    bool thereAreObstaclesOnCurrentDirection_run(double posible_yaw_direction_deg, double distance_to_check = 0.5);
    bool selectNearestIRobot(); // if true there is a nearest iRobot to touch :D
    bool getPositionOfNearestIRobot( double &x_ref, double &y_ref);
    inline std::list<TrackedIRobot> *get_p2_irobot_list() { return &irobot_list; }
};

#endif // IROBOT_FEEDBACK_SUPERVISOR_H
