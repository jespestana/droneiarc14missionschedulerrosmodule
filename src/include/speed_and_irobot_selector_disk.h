#ifndef SPEED_AND_IROBOT_SELECTOR_DISK_H
#define SPEED_AND_IROBOT_SELECTOR_DISK_H

#include "ros/ros.h"
#include "droneMsgsROS/points3DStamped.h"

#include <list>
#include "gridpoints_based_map_border_detector.h"
#include "communication_definition.h"
#include "cvg_random.h"

//Subscribers: iRobot robotPose...
#include "droneMsgsROS/robotPose.h"
#include "droneMsgsROS/robotPoseVector.h"
#include "droneMsgsROS/robotPoseStamped.h"
#include "droneMsgsROS/robotPoseStampedVector.h"

#include "irobot_feedback_supervisor.h"

// Border detection
#define GRBD_THRESHOLD_MIN_DISTANCE             0.80    // m
#define GRBD_THRESHOLD_MAX_DISTANCE             3.50    // m
#define GRBD_THRESHOLD_MAX_ELAPSED_TIME         1.00    // seconds
#define GRBD_THRESHOLD_MIN_COUNT                1.00    // counts

// Border avoidance
#define GRBD_FORBIDDEN_ANGLE_RANGE_BORDER     200.0     // deg

class SpeedAndIRobotSelectorDisk
{
private:
    ros::NodeHandle n;

public:
    SpeedAndIRobotSelectorDisk();
    ~SpeedAndIRobotSelectorDisk();

    void open(ros::NodeHandle & nIn);

private:
    // Gridpoints Based Map Border Detectors
    GridpointsBasedMapBorderDetector grid_detector_front;
    GridpointsBasedMapBorderDetector grid_detector_front_left;
    GridpointsBasedMapBorderDetector grid_detector_left;
    GridpointsBasedMapBorderDetector grid_detector_back_left;
    GridpointsBasedMapBorderDetector grid_detector_back;
    GridpointsBasedMapBorderDetector grid_detector_back_right;
    GridpointsBasedMapBorderDetector grid_detector_right;
    GridpointsBasedMapBorderDetector grid_detector_front_right;

    // Subscribers
    ros::Subscriber gridIntersectionsSub_back;
//    ros::Subscriber gridIntersectionsSub_bottom;
    ros::Subscriber gridIntersectionsSub_left;
    ros::Subscriber gridIntersectionsSub_front;
    ros::Subscriber gridIntersectionsSub_right;
    void gridIntersectionsCallback(const droneMsgsROS::points3DStamped::ConstPtr& msg);

public:
    inline bool isBorderDetected_front()       { return grid_detector_front.isBorderDetected(); }
    inline bool isBorderDetected_front_left()  { return grid_detector_front_left.isBorderDetected(); }
    inline bool isBorderDetected_left()        { return grid_detector_left.isBorderDetected(); }
    inline bool isBorderDetected_back_left()   { return grid_detector_back_left.isBorderDetected(); }
    inline bool isBorderDetected_back()        { return grid_detector_back.isBorderDetected(); }
    inline bool isBorderDetected_back_right()  { return grid_detector_back_right.isBorderDetected(); }
    inline bool isBorderDetected_right()       { return grid_detector_right.isBorderDetected(); }
    inline bool isBorderDetected_front_right() { return grid_detector_front_right.isBorderDetected(); }

public:
    // returns whether there obstacles on current moving direction (stored in currently_advised_yaw_direction_deg)
    bool   thereAreObstaclesOnCurrentDirection_run();
    inline double getCurrentlyAdvisedYawDirectionDeg() { return currently_advised_yaw_direction_deg; }
    double calculateNewAdvisedYawDirectionDeg();
    double returnAValidAdvisedYawDirectionDeg();
private:
    double currently_advised_yaw_direction_deg;     // counter-clockwise as seen from above

public:
    // iRobot Feedback
    IRobotFeedbackSupervisor irobot_feedback_supervisor;
};

#endif // SPEED_AND_IROBOT_SELECTOR_DISK_H
