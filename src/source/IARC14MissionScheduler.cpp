#include "IARC14MissionScheduler.h"

DroneIARC14MissionScheduler::DroneIARC14MissionScheduler() :
    DroneModule( droneModule::active, FREQ_DRONEIARC14MISSIONSCHEDULER),
    iarc14_MS_state_machine()
    {
    init();
    return;
}

DroneIARC14MissionScheduler::~DroneIARC14MissionScheduler() {
    close();
    return;
}

void DroneIARC14MissionScheduler::init() {
    //end
    DroneModule::init();

    return;
}

void DroneIARC14MissionScheduler::open(ros::NodeHandle & nIn, std::string moduleName) {
    //Node
    DroneModule::open(nIn,moduleName);

    iarc14_MS_state_machine.open(n);

    //Flag of module opened
    droneModuleOpened=true;

    return;
}

void DroneIARC14MissionScheduler::close() {
    DroneModule::close();
    return;
}

bool DroneIARC14MissionScheduler::run() {
    DroneModule::run();

    if (isStarted()) {
        bool everything_ok = iarc14_MS_state_machine.run();
//        if (!everything_ok) {
//            stop();
//        }
    }
    return true;
}
