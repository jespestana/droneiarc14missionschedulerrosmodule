#include "IARC14MissionScheduler_statemachine.h"

DroneIARC14MissionSchedulerStateMachine::DroneIARC14MissionSchedulerStateMachine() :
    iarc14_brain_interface(        MODULE_NAME_ARCHITECTURE_BRAIN      , ModuleNames::ARCHITECTURE_BRAIN   ),
    controller_interface(          MODULE_NAME_TRAJECTORY_CONTROLLER   , ModuleNames::TRAJECTORY_CONTROLLER),
    ekf_state_estimator_interface( MODULE_NAME_ODOMETRY_STATE_ESTIMATOR, ModuleNames::ODOMETRY_STATE_ESTIMATOR)
{
    current_state = MissionSchedulerStates::INITIAL_STATE;
    next_state    = MissionSchedulerStates::INITIAL_STATE;
    state_step    = 0;
}

DroneIARC14MissionSchedulerStateMachine::~DroneIARC14MissionSchedulerStateMachine() {
}

void DroneIARC14MissionSchedulerStateMachine::open(ros::NodeHandle & nIn) {
    n = nIn;

    speed_irobot_selector_disk.open(n);

    iarc14_brain_interface.open(n);
    controller_interface.open(n);
    ekf_state_estimator_interface.open(n);

    return;
}

bool DroneIARC14MissionSchedulerStateMachine::run() {
    bool is_successful = true;
    is_successful = processState();
    stateTransitionCheck();
    return is_successful;
}

bool DroneIARC14MissionSchedulerStateMachine::processState()
{
    switch (current_state) {
    case MissionSchedulerStates::INITIAL_STATE:
    {
        switch (state_step) {
        // [state_step=0], go to INIT_SEQUENCE
        case 0:
            next_state = MissionSchedulerStates::INIT_SEQUENCE;
            state_step++;
            break;
            // [state_step=1], state transition step
        case 1:
            break;
            // [state_step=¿?], this state_step should not be reached
        default:
            state_step = 0;
            break;
        }
    }
        break;
    case MissionSchedulerStates::INIT_SEQUENCE:
    {
        switch (state_step) {
        // [state_step=0], send take_off command
        case 0:
            iarc14_brain_interface.newHLCommand( droneMsgsROS::droneHLCommand::TAKE_OFF );
            state_step++;
            break;
            // [state_step=1], wait for take_off to finish
        case 1:
            if ( iarc14_brain_interface.checkLastAck() ) {
                iarc14_brain_interface.unflagAckWaiting();
                next_state = MissionSchedulerStates::HOVERING;
                state_step++;
            }
            break;
            // [state_step=2], state transition step
        case 2:
            break;
            // [state_step=¿?], this state_step should not be reached
        default:
            state_step = 0;
            break;
        }
    }
        break;
    case MissionSchedulerStates::HOVERING:
    {
        switch (state_step) {
        // [state_step=0], go to CONTROL_STARTUP_SEQUENCE
        case 0:
            next_state = MissionSchedulerStates::CONTROL_STARTUP_SEQUENCE;
            state_step++;
            break;
            // [state_step=1], state transition step
        case 1:
            break;
            // [state_step=¿?], this state_step should not be reached
        default:
            state_step = 0;
            break;
        }
    }
        break;
    case MissionSchedulerStates::CONTROL_STARTUP_SEQUENCE:
    {
        switch (state_step) {
        // [state_step=0], send start_controller command
        case 0:
            iarc14_brain_interface.newHLCommand( droneMsgsROS::droneHLCommand::START_CONTROLLER );
            state_step++;
            break;
            // [state_step=1], wait for take_off to finish
        case 1:
            if ( iarc14_brain_interface.checkLastAck() ) {
                iarc14_brain_interface.unflagAckWaiting();
                controller_interface.setControlMode( Controller_MidLevel_controlMode::SPEED_CONTROL );
                next_state = MissionSchedulerStates::IN_FLIGHT_BORDER_BOUNCING;
                state_step++;
            }
            break;
            // [state_step=2], state transition step
        case 2:
            break;
            // [state_step=¿?], this state_step should not be reached
        default:
            state_step = 0;
            break;
        }
    }
        break;
    case MissionSchedulerStates::IN_FLIGHT_BORDER_BOUNCING:
    {
        double valid_yaw_direction_deg = speed_irobot_selector_disk.returnAValidAdvisedYawDirectionDeg();
        if ( fabs(valid_yaw_direction_deg) < 1000.0 ) {
            double valid_yaw_direction_rad = (M_PI/180.0) * valid_yaw_direction_deg;
            double drone_yaw_rad = ekf_state_estimator_interface.last_drone_estimated_GMRwrtGFF_pose_msg().yaw;
            double vlx, vly;
            vlx = cos(valid_yaw_direction_rad) * MS_IN_FLIGHT_OBSTACLE_BOUNCING_SPEED;
            vly = sin(valid_yaw_direction_rad) * MS_IN_FLIGHT_OBSTACLE_BOUNCING_SPEED;
            double vxfi, vyfi;
            vxfi = (+1.0) * cos(drone_yaw_rad) * vlx + (-1.0) * sin(drone_yaw_rad) * vly;
            vyfi = (+1.0) * sin(drone_yaw_rad) * vlx + (+1.0) * cos(drone_yaw_rad) * vly;
            controller_interface.publishDroneSpeedsReference( vxfi, vyfi);
        } else {
            double vxfi, vyfi;
            vxfi = 0.00;
            vyfi = 0.00;
            controller_interface.publishDroneSpeedsReference( vxfi, vyfi);
        }
    }
        break;
    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_REFERENCE_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::PBVS_TRACKER_IS_REFERENCE:
    {
    }
        break;
    case MissionSchedulerStates::TO_IN_FLIGHT_BORDER_BOUNCING_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_FEEDBACK_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::PBVS_TRACKER_IS_FEEDBACK:
    {
    }
        break;
    case MissionSchedulerStates::TO_MOVE_TOWARDS_TOUCH_DOWN_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::MOVE_TOWARDS_TOUCH_DOWN:
    {
    }
        break;
    case MissionSchedulerStates::TOUCH_DOWN_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::FROM_TOUCH_DOWN_SEQUENCE:
    {
    }
        break;
    default:
    {
    }
        break;
    }
    return true;
}

void DroneIARC14MissionSchedulerStateMachine::stateTransitionCheck()
{
    switch (current_state) {
    case MissionSchedulerStates::INITIAL_STATE:
    {
        if ( state_step == 1 ) {
            current_state = next_state;
            state_step = 0;
        }
    }
        break;
    case MissionSchedulerStates::INIT_SEQUENCE:
    {
        if ( state_step == 2 ) {
            current_state = next_state;
            state_step = 0;
        }
    }
        break;
    case MissionSchedulerStates::HOVERING:
    {
        if ( state_step == 1 ) {
            current_state = next_state;
            state_step = 0;
        }
    }
        break;
    case MissionSchedulerStates::CONTROL_STARTUP_SEQUENCE:
    {
        if ( state_step == 2 ) {
            current_state = next_state;
            state_step = 0;
        }
    }
        break;
    case MissionSchedulerStates::IN_FLIGHT_BORDER_BOUNCING:
    {
    }
        break;
    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_REFERENCE_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::PBVS_TRACKER_IS_REFERENCE:
    {
    }
        break;
    case MissionSchedulerStates::TO_IN_FLIGHT_BORDER_BOUNCING_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_FEEDBACK_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::PBVS_TRACKER_IS_FEEDBACK:
    {
    }
        break;
    case MissionSchedulerStates::TO_MOVE_TOWARDS_TOUCH_DOWN_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::MOVE_TOWARDS_TOUCH_DOWN:
    {
    }
        break;
    case MissionSchedulerStates::TOUCH_DOWN_SEQUENCE:
    {
    }
        break;
    case MissionSchedulerStates::FROM_TOUCH_DOWN_SEQUENCE:
    {
    }
        break;
    default:
    {
    }
        break;
    }
}

std::string DroneIARC14MissionSchedulerStateMachine::getMissionScheduler_str() {
    std::string return_str = "";
    switch (current_state) {
    case MissionSchedulerStates::INITIAL_STATE:
        return_str = "INITIAL_STATE";
        break;
    case MissionSchedulerStates::INIT_SEQUENCE:
        return_str = "INIT_SEQUENCE";
        break;
    case MissionSchedulerStates::HOVERING:
        return_str = "HOVERING";
        break;
    case MissionSchedulerStates::CONTROL_STARTUP_SEQUENCE:
        return_str = "CONTROL_STARTUP_SEQUENCE";
        break;
    case MissionSchedulerStates::IN_FLIGHT_BORDER_BOUNCING:
        return_str = "IN_FLIGHT_BORDER_BOUNCING";
        break;
    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_REFERENCE_SEQUENCE:
        return_str = "TO_PBVS_TRACKER_IS_REFERENCE_SEQUENCE";
        break;
    case MissionSchedulerStates::PBVS_TRACKER_IS_REFERENCE:
        return_str = "PBVS_TRACKER_IS_REFERENCE";
        break;
    case MissionSchedulerStates::TO_IN_FLIGHT_BORDER_BOUNCING_SEQUENCE:
        return_str = "TO_IN_FLIGHT_BORDER_BOUNCING_SEQUENCE";
        break;
    case MissionSchedulerStates::TO_PBVS_TRACKER_IS_FEEDBACK_SEQUENCE:
        return_str = "TO_PBVS_TRACKER_IS_FEEDBACK_SEQUENCE";
        break;
    case MissionSchedulerStates::PBVS_TRACKER_IS_FEEDBACK:
        return_str = "PBVS_TRACKER_IS_FEEDBACK";
        break;
    case MissionSchedulerStates::TO_MOVE_TOWARDS_TOUCH_DOWN_SEQUENCE:
        return_str = "TO_MOVE_TOWARDS_TOUCH_DOWN_SEQUENCE";
        break;
    case MissionSchedulerStates::MOVE_TOWARDS_TOUCH_DOWN:
        return_str = "MOVE_TOWARDS_TOUCH_DOWN";
        break;
    case MissionSchedulerStates::TOUCH_DOWN_SEQUENCE:
        return_str = "TOUCH_DOWN_SEQUENCE";
        break;
    case MissionSchedulerStates::FROM_TOUCH_DOWN_SEQUENCE:
        return_str = "TOUCH_DOWN";
        break;
    default:
        return_str = "UNDEFINED";
        break;
    }
    return return_str;
}

std::string DroneIARC14MissionSchedulerStateMachine::getMissionSchedulerStateStep_str() {
    std::ostringstream convert;
    convert << state_step;
    std::string return_str = convert.str();
    return return_str;
}
