#include "gridpoints_based_map_border_detector.h"

GridpointsBasedMapBorderDetector::GridpointsBasedMapBorderDetector(double direction_yaw_deg_in, double threshold_min_distance_in,
                                                                   double threshold_max_distance_in, double threshold_max_time_in, int threshold_min_count_in)
{
    direction_yaw_deg      = direction_yaw_deg_in;
    threshold_min_distance = threshold_min_distance_in;
    threshold_max_distance = threshold_max_distance_in;
    threshold_min_count    = threshold_min_count_in;
    threshold_max_time     = threshold_max_time_in;
    last_valid_measurement_distance  = 0.00;
    last_valid_measurement_timestamp = ros::Time::now();
}

GridpointsBasedMapBorderDetector::GridpointsBasedMapBorderDetector()
{
    direction_yaw_deg      = 0.00;
    threshold_min_distance = 0.80;
    threshold_max_distance = 3.50;
    threshold_min_count    = 1;
    threshold_max_time     = 1.00;
    last_valid_measurement_distance  = 0.00;
    last_valid_measurement_timestamp = ros::Time::now();
}

GridpointsBasedMapBorderDetector::~GridpointsBasedMapBorderDetector()
{
}

void GridpointsBasedMapBorderDetector::setConfigurationParameters(double direction_yaw_deg_in, double threshold_min_distance_in,
                                                                  double threshold_max_distance_in, double threshold_max_time_in, int threshold_min_count_in)
{
    direction_yaw_deg      = direction_yaw_deg_in;
    threshold_min_distance = threshold_min_distance_in;
    threshold_max_distance = threshold_max_distance_in;
    threshold_min_count    = threshold_min_count_in;
    threshold_max_time     = threshold_max_time_in;
    last_valid_measurement_timestamp = ros::Time::now();
}

void GridpointsBasedMapBorderDetector::calculateBorderDetection(const droneMsgsROS::points3DStamped::ConstPtr &msg)
{
    // direction in which distance is measured
    double direction_yaw_rad = (M_PI/180.0) * direction_yaw_deg;
    double ux = cos(direction_yaw_rad), uy = sin(direction_yaw_rad);

    int count = 0;
    for (std::vector<droneMsgsROS::vector3f>::const_iterator it = msg->points3D.begin();
         it != msg->points3D.end();
         ++it) {

        double distance_with_sign = (it->x * ux) + ((it->y * uy));
        if ( (threshold_min_distance < distance_with_sign) && (distance_with_sign < threshold_max_distance) ) {
            count++;
            if (count >= threshold_min_count) {
                last_valid_measurement_distance  = distance_with_sign; // this parameter is not very useful...
                last_valid_measurement_timestamp = msg->header.stamp;
                break;
            }
        }
    }
}

bool GridpointsBasedMapBorderDetector::isBorderDetected() {
    ros::Duration elapsed_time = ros::Time::now() - last_valid_measurement_timestamp;
    return ( elapsed_time.toSec() >= threshold_max_time );
}

bool GridpointsBasedMapBorderDetector::thisDirectionIsNotAllowed(double currently_advised_yaw_direction_deg, double GRBD_FORBIDDEN_ANGLE_RANGE_BORDER)
{
    double max_allowed_angle_diff = (GRBD_FORBIDDEN_ANGLE_RANGE_BORDER * (M_PI/180.0)) / 2.0;
    double angle_diff = cvg_utils_library::getAngleError( currently_advised_yaw_direction_deg * (M_PI/180.0), direction_yaw_deg * (M_PI/180.0));
    return ( fabs(angle_diff) <= max_allowed_angle_diff );
}
