#include "irobot_feedback_supervisor.h"

IRobotFeedbackSupervisor::IRobotFeedbackSupervisor()
{
}

IRobotFeedbackSupervisor::~IRobotFeedbackSupervisor() {
}

void IRobotFeedbackSupervisor::open(ros::NodeHandle & nIn) {
    n = nIn;

    // Subscription to MulticameraIRobotTracker feedback
    iRobotsPoseTrackerFeedbackSubs = n.subscribe( MULTICAMERA_IROBOT_TRACKER_FEEDBACK, 1, &IRobotFeedbackSupervisor::iRobotsPoseTrackerPoseCallback, this);

}

void IRobotFeedbackSupervisor::iRobotsPoseTrackerPoseCallback(const droneMsgsROS::robotPoseStampedVector::ConstPtr &msg)
{
    // Delete the elements (irobots) of irobot_list that are not in msg->robot_pose_vector
    for (std::list<TrackedIRobot>::iterator it_i = irobot_list.begin();
         it_i != irobot_list.end();
         it_i++) {
        bool irobot_is_on_msg = false;
        for (std::vector<droneMsgsROS::robotPoseStamped>::const_iterator it_j = msg->robot_pose_vector.begin();
             it_j != msg->robot_pose_vector.end();
             it_j++) {
            if ( (*it_i).getIdRobot() == (*it_j).id_Robot ) {
                irobot_is_on_msg = true;
                break;
            }
        }
        if (!irobot_is_on_msg) {
            std::list<TrackedIRobot>::iterator it_aux = it_i;
            it_aux++;
            irobot_list.erase(it_i);
            it_i = it_aux;
        }
    }

    // update or append irobots to irobot_list
    for (std::vector<droneMsgsROS::robotPoseStamped>::const_iterator it_i = msg->robot_pose_vector.begin();
         it_i != msg->robot_pose_vector.end();
         it_i++) {
        bool irobot_is_on_msg = false;
        for (std::list<TrackedIRobot>::iterator it_j = irobot_list.begin();
             it_j != irobot_list.end();
             it_j++) {
            // Update the elements (irobots) of irobot_list with the elements (iRobots) in msg->robot_pose_vector
            if ( (*it_i).id_Robot == (*it_j).getIdRobot() ) {
                irobot_is_on_msg = true;
                (*it_j).updateRobotInformation( *it_i, false);
                break;
            }
        }
        // if new: add irobot to irobot_list
        if (!irobot_is_on_msg) {
            TrackedIRobot this_irobot( (*it_i).id_Robot, (*it_i).Robot_Type);
            this_irobot.updateRobotInformation( (*it_i), true);
            irobot_list.push_back(this_irobot);
        }
    }
}

bool IRobotFeedbackSupervisor::thereAreObstaclesOnCurrentDirection_run(double posible_yaw_direction_deg, double distance_to_check)
{
    double max_allowed_angle_diff = (IRFS_FORBIDDEN_ANGLE_RANGE_COLUMN * (M_PI/180.0)) / 2.0;
    bool there_is_an_obstacle = false;
    for (std::list<TrackedIRobot>::iterator it_i = irobot_list.begin();
         it_i != irobot_list.end();
         it_i++) {
        if ( (*it_i).isUnderPassiveFeedback() ) {
            float r_x, r_y;
            (*it_i).getPosition( r_x, r_y);
            double distance_to_this_robot = sqrt( r_x*r_x + r_y*r_y );
            if ( ( (*it_i).getRobotType() <= 0 ) && ( distance_to_this_robot > distance_to_check ) ) {
                double direction_yaw_rad = atan2( r_y, r_x);
                double angle_diff = cvg_utils_library::getAngleError( posible_yaw_direction_deg * (M_PI/180.0), direction_yaw_rad);
                there_is_an_obstacle = ( fabs(angle_diff) <= max_allowed_angle_diff );
                if (there_is_an_obstacle) {
                    break;
                }
            }
        }

    }
    return there_is_an_obstacle;
}

bool IRobotFeedbackSupervisor::selectNearestIRobot()
{

}

bool IRobotFeedbackSupervisor::getPositionOfNearestIRobot(double &x_ref, double &y_ref)
{

}
