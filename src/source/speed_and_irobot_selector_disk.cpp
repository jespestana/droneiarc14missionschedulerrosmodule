#include "speed_and_irobot_selector_disk.h"

SpeedAndIRobotSelectorDisk::SpeedAndIRobotSelectorDisk() :
grid_detector_front(         0.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT),
grid_detector_front_left(   45.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT),
grid_detector_left(         90.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT),
grid_detector_back_left(   135.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT),
grid_detector_back(        180.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT),
grid_detector_back_right(  225.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT),
grid_detector_right(       270.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT),
grid_detector_front_right( 315.00, GRBD_THRESHOLD_MIN_DISTANCE, GRBD_THRESHOLD_MAX_DISTANCE, GRBD_THRESHOLD_MAX_ELAPSED_TIME, GRBD_THRESHOLD_MIN_COUNT)
{
    currently_advised_yaw_direction_deg = cvg_uniform_random( -180.00, 180.00);
}

SpeedAndIRobotSelectorDisk::~SpeedAndIRobotSelectorDisk()
{
}

void SpeedAndIRobotSelectorDisk::open(ros::NodeHandle &nIn)
{
    n = nIn;

    // Subscribers
    gridIntersectionsSub_back   = n.subscribe( GRID_INTERSECTIONS_TOPIC_BACK,   1, &SpeedAndIRobotSelectorDisk::gridIntersectionsCallback, this);
//    gridIntersectionsSub_bottom = n.subscribe( GRID_INTERSECTIONS_TOPIC_BOTTOM, 1, &SpeedAndIRobotSelectorDisk::gridIntersectionsCallback, this);
    gridIntersectionsSub_left   = n.subscribe( GRID_INTERSECTIONS_TOPIC_LEFT,   1, &SpeedAndIRobotSelectorDisk::gridIntersectionsCallback, this);
    gridIntersectionsSub_front  = n.subscribe( GRID_INTERSECTIONS_TOPIC_FRONT,  1, &SpeedAndIRobotSelectorDisk::gridIntersectionsCallback, this);
    gridIntersectionsSub_right  = n.subscribe( GRID_INTERSECTIONS_TOPIC_RIGHT,  1, &SpeedAndIRobotSelectorDisk::gridIntersectionsCallback, this);

    irobot_feedback_supervisor.open(n);
}

void SpeedAndIRobotSelectorDisk::gridIntersectionsCallback(const droneMsgsROS::points3DStamped::ConstPtr &msg)
{
    // If the new measurement is not empty, then pass it to all the border detectors
    if (!msg->points3D.empty()) {
        grid_detector_front.calculateBorderDetection( msg );
        grid_detector_front_left.calculateBorderDetection( msg );
        grid_detector_left.calculateBorderDetection( msg );
        grid_detector_back_left.calculateBorderDetection( msg );
        grid_detector_back.calculateBorderDetection( msg );
        grid_detector_back_right.calculateBorderDetection( msg );
        grid_detector_right.calculateBorderDetection( msg );
        grid_detector_front_right.calculateBorderDetection( msg );
    }
}

bool SpeedAndIRobotSelectorDisk::thereAreObstaclesOnCurrentDirection_run()
{
    bool borders_obstacle = false;
    bool columns_obstacle = false;

    bool wall_in_front       = grid_detector_front.isBorderDetected()       ? grid_detector_front.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg      , GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;
    bool wall_in_front_left  = grid_detector_front_left.isBorderDetected()  ? grid_detector_front_left.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg , GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;
    bool wall_in_left        = grid_detector_left.isBorderDetected()        ? grid_detector_left.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg       , GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;
    bool wall_in_back_left   = grid_detector_back_left.isBorderDetected()   ? grid_detector_back_left.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg  , GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;
    bool wall_in_back        = grid_detector_back.isBorderDetected()        ? grid_detector_back.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg       , GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;
    bool wall_in_back_right  = grid_detector_back_right.isBorderDetected()  ? grid_detector_back_right.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg , GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;
    bool wall_in_right       = grid_detector_right.isBorderDetected()       ? grid_detector_right.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg      , GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;
    bool wall_in_front_right = grid_detector_front_right.isBorderDetected() ? grid_detector_front_right.thisDirectionIsNotAllowed( currently_advised_yaw_direction_deg, GRBD_FORBIDDEN_ANGLE_RANGE_BORDER ) : false;

    borders_obstacle = borders_obstacle   ||
                       wall_in_front      ||
                       wall_in_front_left ||
                       wall_in_left       ||
                       wall_in_back_left  ||
                       wall_in_back       ||
                       wall_in_back_right ||
                       wall_in_right      ||
                       wall_in_front_right;

    return (borders_obstacle || columns_obstacle);
}

double SpeedAndIRobotSelectorDisk::calculateNewAdvisedYawDirectionDeg()
{
    currently_advised_yaw_direction_deg = cvg_uniform_random( -180.00, 180.00);
    for (int i=0; i<=50; i++) {
        if ( thereAreObstaclesOnCurrentDirection_run() ) {
            currently_advised_yaw_direction_deg = cvg_uniform_random( -180.00, 180.00);
        } else {
            break;
        }
    }

    return currently_advised_yaw_direction_deg;
}

double SpeedAndIRobotSelectorDisk::returnAValidAdvisedYawDirectionDeg()
{
    bool solution_found = false;
    for (int i=0; i<=50; i++) {
        if ( ( fabs(currently_advised_yaw_direction_deg) > 1000.0 ) || thereAreObstaclesOnCurrentDirection_run() || irobot_feedback_supervisor.thereAreObstaclesOnCurrentDirection_run( currently_advised_yaw_direction_deg, 1.0)) {
            currently_advised_yaw_direction_deg = cvg_uniform_random( -180.00, 180.00);
        } else {
            solution_found = true;
            break;
        }
    }

    if ( !solution_found ) {
        currently_advised_yaw_direction_deg = -2000.0;
    }

    return currently_advised_yaw_direction_deg;
}

